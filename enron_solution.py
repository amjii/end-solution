import pandas as pd
import numpy as np
from email.utils import parsedate_tz, mktime_tz
from time import time, strptime
import re
import os
from pathos.pools import ProcessPool
from helper_funcs import *

# Get all emails by employee
print("Collect files to parse per employee:")
start = time()
employees = os.listdir("maildir")
file_locations = flatMap(
    [getDirFiles(e, "maildir/{}".format(e)) for e in employees])
print("Finished in: {}".format(time() - start))

# Process the emails in chunks of 50k
pool = ProcessPool()
chunks = list(chunkify(file_locations, 50000))
print("Process files by chunks: total {} chunks:".format(len(chunks)))
for i, chunk in enumerate(chunks):
    print("Will process chunk {} / {}".format(i+1, len(chunks)))
    start = time()
    contents_parsed = list(
        pool.imap(lambda x: regParseEmail(x[0], readFile(x[1]), x[1]), chunk))

    print("Finished in: {} seconds.".format(time() - start))

    contents_flat = flatMap(
        map(lambda e: e[1], filter(lambda e: e[0], contents_parsed))
        )

    contents_df = pd.DataFrame.from_records(contents_flat, 
        columns=["eid","email_date", "from", "to", "path"])

    contents_df.to_csv("parsed/enron_emails_chunk_{}.csv".format(i), sep="\t", index=False)

# Load all chunks into a unified dataframe
file_chunks = ["parsed/{}".format(f) for f in os.listdir("parsed") if re.search("enron_emails_chunk_", f)]
enron_df = pd.concat([pd.read_csv(f, sep="\t") for f in file_chunks])

# Count number of emails sent from each sender to each recipient and save to csv
from_to_counts = enron_df[["from", "to"]]\
.groupby(["from", "to"]).size().reset_index()

from_to_counts.columns = ["sender", "recipient", "count"]
from_to_counts.to_csv("outputs/from_to_counts.csv", index=False)

# Count average number of emails received by each employee id by weekday

# parse timestamps to utc
enron_df["email_date_parsed"] = pd.to_datetime(enron_df["email_date"].str.slice(0,-6).apply(lambda x: mktime_tz(parsedate_tz(x))), unit="s")
enron_df["day_of_week"] = enron_df["email_date_parsed"].dt.strftime("%u").astype(int) -1
enron_df["email_date_group"] = enron_df["email_date_parsed"].dt.strftime("%Y-%m-%d")

# Count emails by date per employee
enron_received_emails = enron_df[enron_df["path"].str.contains("inbox")]
enron_received_emails_by_date = enron_received_emails[["eid", "day_of_week", "email_date_group"]]\
.groupby(["eid", "day_of_week", "email_date_group"]).size().reset_index()
enron_received_emails_by_date.columns = ["eid", "day_of_week", "email_date_group", "count"]

# Count average emails per employee on a weekday
weekday_averages = enron_received_emails_by_date[["eid", "day_of_week", "count"]]\
.groupby(["eid", "day_of_week"]).mean().reset_index()
weekday_averages.columns = ["employee", "day_of_week", "average"]
weekday_averages.to_csv("outputs/weekday_averages.csv", index=False)

