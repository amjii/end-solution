Enron email challenge solution
=============

Requirements
-----------

Implemented in Python 3.6.0 (default, Mar  4 2017, 12:32:34)

Package requirements in requirements.txt, to install using pip:

```
pip install -r requirements.txt
```

Running the solution
-----------

1. Fetch and untar the data by running the script get-data.sh in bash or such.

1. To run the data parsing and aggregations, just call: python enron-solution.py

1. The parsed chunks of data are output to the "parsed" folder

1. Aggregated data is output to the "outputs folder"

Examples of aggregate outputs are located currently in the outputs folder

Assumptions made in the solution
-----------

* counts don't have to be unique counts

* received emails per employee resides in their inbox (in practice, they might reside in any of the folders)