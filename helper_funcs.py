from itertools import chain
import re
import os
import codecs

# Regex expressions used in parsing
rgx_subject = re.compile("Subject:")
rgx_date = re.compile("Date:.+\n")
rgx_email = re.compile(".+@.+")
rgx_from = re.compile("From:.+\n")
rgx_to = re.compile("To:.+\n")
rgx_cc = re.compile("Cc:.+\n")
rgx_bcc = re.compile("Bcc:.+\n")

def flatMap(list_of_lists):
    """Combine lists of lists into a flat list"""
    return list(chain.from_iterable(list_of_lists))

def getDirFiles(eid, rootpath):
    """get absolute paths to all files within a root path"""
    found_files = [[(eid, os.path.abspath(os.path.join(fol, f))) for f in files]
    for (fol, subfols, files) in  os.walk(rootpath)]
    return flatMap(found_files)

def cleanField(entry, pattern):
    """Clean a single field to be parsed, given field and rgx pattern to clean"""
    if not entry:
        return entry
    return re.sub(pattern, "", entry[0]).strip()

def splitNestedEmails(field):
    """Split nested email field into a list"""
    if field:
        if "," in field:
            return field.replace(" ", "").split(",")
        if ";" in field:
            return field.replace(" ", "").split(";")
    return [field]

def getAllRecipients(to, cc, bcc):
    """Combine recipients (nested to, cc and bcc fields) into a flat list"""
    to_entries = splitNestedEmails(to)
    cc_entries = splitNestedEmails(cc)
    bcc_entries = splitNestedEmails(bcc)
    return list(set(
        filter(lambda x: x and rgx_email.search(x), flatMap([to_entries, cc_entries, bcc_entries]))
        ))

def regParseEmail(eid, cont, path):
    """Parses a single email content into the subset of fields of interest."""

    if not cont:
        return (False, ())
    try:
        email_date = cleanField(rgx_date.search(cont), r"Date:")
        from_addr = cleanField(rgx_from.search(cont), r"From:")
        to_addr = cleanField(rgx_to.search(cont), r"To:")
        cc_addr = cleanField(rgx_cc.search(cont), r"Cc:")
        bcc_addr = cleanField(rgx_bcc.search(cont), r"Bcc:")
        all_recipients = getAllRecipients(to_addr, cc_addr, bcc_addr)

        # Produce a result row for each from - to pair
        return (True, 
            [(eid, email_date, from_addr, to_a, path)
            for to_a in all_recipients])

    except Exception as e:
        return (False, ())

def readFile(fname):
    """read a single file"""
    try:
        with codecs.open(fname, "r", encoding="cp1252") as f:
            return f.read()
    except Exception as e:
        pass

def parseEmail(eid, cont):
    """
    Parses a single email content into the subset of fields of interest. 

    returns processing result in form:
    (ok, (employee, emailtimestamp, from_addr, to_addr, md5hash))"""

    if not cont:
        return (False, ())

    msg = email.message_from_string(cont)
    if not msg:
        return (False, ())

    email_date = msg.get("Date")
    from_addr = msg.get("From")
    to_addr = msg.get("To", "").replace("\n", "").replace("\r", "")
    emailid = msg.get("Message-ID")

    # If email has multiple recipients
    if (to_addr and "," in to_addr):
        return (True, 
            [(eid, email_date, from_addr, to_a, emailid)
            for to_a in to_addr.split(",")])

    return (True, [(eid, email_date, from_addr, to_addr, emailid)])

def chunkify(l, n):
    """Split a list to n chunks"""
    for i in range(0, len(l), n):
        yield l[i:i + n]